# Dummy HTML file

## images 

```
for ((i = 0; i < 200; i++)); do
curl http://lorempixel.com/400/200/ > dummy-${i}.jpg
done
```

## HTML files

```
for ((i = 0 ; i < 100; i++)); do
sed -e "s/placeholder.jpg/dummy-$((i * 2)).jpg/" -e "s/placeholder_.jpg/dummy-$((i * 2 + 1)).jpg/" dummy.html > dummy-${i}.html
done
```
